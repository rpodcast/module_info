__EDIT__: There is a much more sane approach to this concept that I will explore thanks to key input from Joe Cheng!  Just putting this out there as my effort to "learn out loud"

Documenting my ideas for how to successfully migrate away from using the :potable_water: approach of passing a massive reactive values object (referred to as `r_data`) and saving/loading that object whenever key events occur:

### Updating reactive values among multiple modules

Instead of having one massive `reactiveValues` object, we should be explicit about the kinds of objects passing back and forth between modules using `reactiveVal()` instead.  Thanks to clever use of `reactiveVal` within a _parent_ environment (or perhaps parent module) as well as in a specific module, combined with using `observe` to track the state, we can successfully keep a given object up-to-date even when multiple process (i.e. modules) could modify it.  

Consider a toy example (adapted from [this app](http://shiny.rstudio.com/gallery/module-example.html)) containing two modules that each update a reactive value called `r1`.  One is a simple plot interaction module, the other is a button click.  The server-side components of these modules take a parameter called r1 (what it does with the initial value is beyond the scope of this particular topic).  They key point is each of these modules have a reactive value placeholder which is updated whenever key actions in these modules occur (button click or plot area selection).  Each module will then return the contents of these reactive value placeholders as part of the list object that comes out.  

In the parent environment (in this case the typical server component of the shiny app):
* Reactive value `r1` is set at an initial value
* Each of the modules are executed and their results saved as placeholders.
* Two `observe` blocks are made that each will update the `r1` reactive value with the contents of the return object for a particular module.  The key takeaway is __you cannot have one observe block that has references to both of the module return values!__   The reason is that whatever is "last" in the block would take precendence, and we want to make sure we get the `r1` value from whichever module was executed last, no matter what it is.
* The app below shows that every time an update occurs for `r1`, the updated value is passed appropriately to each of the two modules.  Then if one module performs an action, that value created within the module is then propagated to the other module.


```r
sillyUI <- function(id) {
  ns <- NS(id)
  actionButton(ns("clicker"), "Click me!")
}

silly <- function(input, output, session, r1) {
  # reactive value placeholder
  r1r <- reactiveVal(1)
  
  observeEvent(input$clicker, {
    r1r(rnorm(1))
  })
  
  return(
    list(
      r1 = r1r
    )
  )
}

linkedScatterUI <- function(id) {
  ns <- NS(id)
  
  fluidRow(
    column(6, plotOutput(ns("plot1"), brush = ns("brush"))),
    column(6, plotOutput(ns("plot2"), brush = ns("brush")))
  )
}

linkedScatter <- function(input, output, session, data, left, right, r1, r2) {
  # Yields the data frame with an additional column "selected_"
  # that indicates whether that observation is brushed
  
  # set up reactive vals within this module
  r1r <- reactiveVal(1)
  r2r <- reactiveVal(1)
  
  dataWithSelection <- reactive({
    brushedPoints(data(), input$brush, allRows = TRUE)
  })
  
  output$plot1 <- renderPlot({
    r1r(rnorm(1))
    scatterPlot(dataWithSelection(), left())
  })
  
  output$plot2 <- renderPlot({
    r2r(rnorm(1))
    scatterPlot(dataWithSelection(), right())
  })
  
  return(
    list(
      dataWithSelection = dataWithSelection,
      r1 = r1r,
      r2 = r2r
    )
  )
}

scatterPlot <- function(data, cols) {
  ggplot(data, aes_string(x = cols[1], y = cols[2])) +
    geom_point(aes(color = selected_)) +
    scale_color_manual(values = c("black", "#66D65C"), guide = FALSE)
}

ui <- fixedPage(
  h2("Module example"),
  linkedScatterUI("scatters"),
  sillyUI("clickermod"),
  textOutput("summary"),
  h2("reactive stuff"),
  verbatimTextOutput("reactivestuff")
)

server <- function(input, output, session) {
  
  # some placeholder reactive values
  r1 <- reactiveVal(1)
  r2 <- reactiveVal(1)
  rvals <- reactiveValues()
  
  clickerres <- callModule(silly, "clickermod", r1 = r1)
  
  dfres <- callModule(linkedScatter, "scatters", reactive(mpg),
                   left = reactive(c("cty", "hwy")),
                   right = reactive(c("drv", "hwy")),
                   r1 = r1,
                   r2 = r2
  )
  
  # observer to have the "last" value of r1
  # KEY POINT: Need to have separate observers for each way that r1 could be updated
  # - i.e. if there are two modules that update it, need to take their return values
  #   separately and update the "server" r1 reactiveVal in each observer
  observe({
    r1(dfres$r1())
  })
  
  observe({
    r1(clickerres$r1())
  })
  
  output$summary <- renderText({
    glue::glue("{nobs} observation(s) selected", nobs = nrow(dplyr::filter(dfres$dataWithSelection(), selected_)))
  })
  
  output$reactivestuff <- renderPrint({
    glue::glue("
      r1 from the overall observer is {r1valobs},
      r1 supplied to linkedScatter is {r1valsupplied},
      r1 from linkedScatter is {r1val}, 
      r1 supplied to silly is {r1valsupplied_silly},
      r1 from silly is {r1valsilly},
      r2 supplied to linkedScatter is {r2valsupplied},
      r2 from linkedScatter is {r2val}",
      r1valobs = r1(),
      r1val = dfres$r1(),
      r1valsilly = clickerres$r1(),
      r2val = dfres$r2(),
      r1valsupplied_silly = clickerres$r1_supplied(),
      r1valsupplied = dfres$r1_supplied(),
      r2valsupplied = dfres$r2_supplied())
  })
}

shinyApp(ui, server)
```